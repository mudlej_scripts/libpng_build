import os
import shutil
from common import run_cmd, log, error, LIBPNG, delete_if_exists

# Toolchain path
NDK_PATH = "/opt/android-ndk/"
TOOLCHAIN = os.path.join(NDK_PATH, "build/cmake/android.toolchain.cmake")
ANDROID_PLATFORM = "16"

# Install path
INSTALL_PATH = "../../lib/"


def run():
  log("Start " + __file__)
  log("INSTALL_PATH: " + INSTALL_PATH)

  build("x86_64", "Release")
  build("arm64-v8a", "Release")
  build("x86", "Release")
  build("armeabi-v7a", "Release")


#------------------------------------------------------------

def build(arch, build_type):
    """
    arch        ->  "x86_64",  "arm64-v8a", ...
    build_type  ->  "Debug" or "Release"
    """
    log("Building libpng arch=" + arch + " build_type=" + build_type)
    INSTALL_PREFIX = os.path.join(INSTALL_PATH, arch)
    os.chdir(LIBPNG)

    # remove existing builds
    shutil.rmtree("build", ignore_errors=True)
    
    # create build folder
    os.makedirs("build", exist_ok=True)
    os.chdir("build")

    # cmake generator
    cmd = ["cmake"]
    cmd += ["-DCMAKE_TOOLCHAIN_FILE=" + TOOLCHAIN]
    cmd += ["-DANDROID_ABI=" + arch]
    cmd += ["-DANDROID_PLATFORM=" + ANDROID_PLATFORM]
    cmd += ["-DCMAKE_INSTALL_PREFIX=" + INSTALL_PREFIX]
    cmd += ["-DPNG_TESTS=OFF -DHAVE_LD_VERSION_SCRIPT=OFF"]

    if build_type == "Debug":
        cmd += ["-DPNG_DEBUG=ON"]
    cmd += [".."]
    run_cmd(cmd)

    # cmake build
    cmd = ["cmake --build ."]
    cmd += ["--config " + build_type]
    run_cmd(cmd)

    # cmake install
    cmd = ["cmake --install ."]
    cmd += ["--config " + build_type]
    run_cmd(cmd)

    log(f"Finished building {arch}")
    
    # move the shared lib 
    shutil.move("libpng16.so", f"../../../../lib/{arch}/libmodpng.so")

    # get back to the main dir
    os.chdir("../../")
    
    # remove other builds
    delete_if_exists(f"{LIBPNG}/build")
    # delete_if_exists("lib")
    # delete_if_exists("build")


if __name__ == '__main__':
    run()