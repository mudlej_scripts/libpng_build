from common import LIBPNG, delete_if_exists, download_file, extract_tar_file
import os

# ---------------------------- Variables

archs = ['armeabi-v7a', 'x86', 'x86_64', 'arm64-v8a']
lib_name, lib_version = LIBPNG.split("-")
download_url = f"https://sourceforge.net/projects/libpng/files/libpng16/{lib_version}/{lib_name}-{lib_version}.tar.xz/download"
tar_file_name = f"{LIBPNG}.tar.xz"

# ---------------------------- Main Code

def run():
    # delete any existing builds
    
    # TODO: fix this, it will delete other libs
    delete_if_exists("../../lib")
    # delete_if_exists("../../../libs")
    # delete_if_exists("../../../obj")

    # create the folders 
    os.mkdir("../../lib/")
    for arch in archs:
        os.mkdir(f"../../lib/{arch}")

    # download and extract the lib tar file
    download_file(download_url, tar_file_name)
    extract_tar_file(tar_file_name)
    os.remove(tar_file_name)


if __name__ == '__main__':
    run()