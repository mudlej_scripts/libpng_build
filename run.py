from common import delete_if_exists, LIBPNG
import prepare, build_android

prepare.run()
build_android.run()
delete_if_exists(LIBPNG)
